package BeloteGame.player;

import BeloteGame.types.Card;
import BeloteGame.types.announce.Announce;

import java.util.List;

/**
 * Created by ThinkPad on 14.5.2017 г..
 */
public class Player {
    private String name;
    private List<Announce> announces;
    private List<Card> handCards;
    private Team team;
}
