package BeloteGame.player;

import BeloteGame.types.announce.Announce;

import java.util.Iterator;
import java.util.List;

/**
 * Created by ThinkPad on 14.5.2017 г..
 */
public class Team {
    private int score;
    private List<Announce> announces;

    public int getTeamScore() {
        return score;
    }

    public Iterator<Announce> getAnnounceIterator()
    {
        return announces.iterator();

    }


}
