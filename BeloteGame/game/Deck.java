package BeloteGame.game;

import BeloteGame.types.Card;
import BeloteGame.types.Rank;
import BeloteGame.types.Suit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by ThinkPad on 14.5.2017 г..
 */
public class Deck {
    private List<Card> cards;

    public Deck()
    {
        cards=new ArrayList<>();
        for (int i = 0; i < Rank.values().length; i++) {
            for (int j = 0; j < Suit.values().length; j++) {
                cards.add(new Card(Rank.values()[i],Suit.values()[j]));
            }
        }
        shuffleDeck();
    }

    public void shuffleDeck()
    {
        Collections.shuffle(cards);
    }
}
