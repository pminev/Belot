package BeloteGame.game.belote;

import BeloteGame.types.Rank;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ThinkPad on 14.5.2017 г..
 */
public class RankValues {
    public static final Map<Rank,Integer> allTrumps=createAllTrumps();
    public static final Map<Rank,Integer> noTrumps=createNoTrumps();

    private static Map<Rank, Integer> createNoTrumps() {
        Map<Rank, Integer> result = new HashMap<Rank, Integer>();
        result.put(Rank.SEVEN, 0);
        result.put(Rank.EIGHT, 0);
        result.put(Rank.NINE, 0);
        result.put(Rank.JACK, 2);
        result.put(Rank.QUENN, 3);
        result.put(Rank.KING, 4);
        result.put(Rank.TEN, 10);
        result.put(Rank.ACE, 11);
        return Collections.unmodifiableMap(result);
    }

    private static Map<Rank, Integer> createAllTrumps() {
        Map<Rank, Integer> result = new HashMap<Rank, Integer>();
        result.put(Rank.SEVEN, 0);
        result.put(Rank.EIGHT, 0);
        result.put(Rank.QUENN, 3);
        result.put(Rank.KING, 4);
        result.put(Rank.TEN, 10);
        result.put(Rank.ACE, 11);
        result.put(Rank.NINE, 14);
        result.put(Rank.JACK, 20);
        return Collections.unmodifiableMap(result);
    }

}
