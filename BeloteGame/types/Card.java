package BeloteGame.types;

/**
 * Created by ThinkPad on 14.5.2017 г..
 */
public class Card {
    private Suit suit;
    private Rank rank;

    public Card(Rank rank,Suit suit)
    {
        this.suit=suit;
        this.rank=rank;
    }

    public boolean isTrump(Bid bid)
    {
        return suit==bid.getSuit();
    }
}
