package BeloteGame.types;

/**
 * Created by ThinkPad on 14.5.2017 г..
 */
public enum Suit {
    CLUBS, DIAMONDS, HEARTS, SPADES
}
